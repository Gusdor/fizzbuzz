using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System.Linq;
using System;

namespace FizzBuzz.Tests
{
    [TestClass]
    public class GameShould
    {
        [TestMethod]
        public void Generate100Results()
        {
            var results = FizzBuzz.Game.Play(100);

            results.Count().Should().Be(100);
        }

        [TestMethod]
        public void BeConstrainedToARangeOfNumbers()
        {
            var results = FizzBuzz.Game.Play(15, 11);

            results.Should().HaveCount(5);
            results.Should().ContainInOrder("11", "Fizz", "13", "14", "FizzBuzz");
        }

        [TestMethod]
        public void Generate15AccurateFizzBuzzResults()
        {
            var results = FizzBuzz.Game.Play(15);

            results.Should().HaveCount(15);
            results.Should().ContainInOrder("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz");
        }

        [TestMethod]
        public void UseAnAlternativeRuleset()
        {
            var ruleset = new OddEvenRules();

            var results = FizzBuzz.Game.Play(Enumerable.Range(1, 5), ruleset);

            results.Should().HaveCount(5);
            results.Should().ContainInOrder(
                OddEvenRules.Result.Odd, 
                OddEvenRules.Result.Even, 
                OddEvenRules.Result.Odd, 
                OddEvenRules.Result.Even,
                OddEvenRules.Result.Odd);
        }
    }
}
