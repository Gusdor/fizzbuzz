﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzz
{
    public class OddEvenRules: IRuleset<OddEvenRules.Result, OddEvenRules.Result>
    {
        public (Func<int, Result>, Predicate<int>)[] Rules => new (Func<int, Result>, Predicate<int>)[]
            {
                (i => Result.Odd, i => i % 2 != 0),
                (i => Result.Even, i => i % 2 == 0),
            };

        public Result Combine(int value, IEnumerable<Result> ruleOutput)
        {
            return ruleOutput.First();
        }

        public enum Result
        {
            Odd,
            Even
        }

    }
}
