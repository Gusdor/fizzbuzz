﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzz
{
    /// <summary>
    /// The standard fizz buzz rules.
    /// </summary>
    public class FizzBuzzRuleset : IRuleset<string, string>
    {
        public (Func<int, string>, Predicate<int>)[] Rules =>
            new (Func<int, string>, Predicate<int>)[]
            {
                (i => "Fizz", i => i % 3 == 0),
                (i => "Buzz", i => i % 5 == 0)
            };

        public string Combine(int value, IEnumerable<string> ruleOutput)
        {
            if (ruleOutput.Any())
            {
                return string.Join(string.Empty, ruleOutput);
            }
            else
            {
                return value.ToString();
            }
        }
    }
}
