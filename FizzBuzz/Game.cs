﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzz
{
    /// <summary>
    /// Runs a game of Fizzbuzz.
    /// </summary>
    public class Game
    {
        static readonly IRuleset<string, string> defaultRules = new FizzBuzzRuleset();

        /// <summary>
        /// Plays a game of fizzbuzz using a range of numbers.
        /// </summary>
        /// <param name="max">The number to play up to.</param>
        /// <param name="min">The number to start the game from.</param>
        /// <returns>A set of results</returns>
        public static IEnumerable<string> Play(int max, int min = 1)
        {
            return Play(Enumerable.Range(min, max-min+1), defaultRules);
        }

        /// <summary>
        /// Plays a counting game using a sequence of numbers and matching rules.
        /// </summary>
        /// <param name="range">The sequence of numbers to use.</param>
        /// <param name="ruleSet">The ruleset that defines the resulting value of each number.</param>
        /// <returns>A set of results.</returns>
        public static IEnumerable<TResult> Play<TResult, TRule>(IEnumerable<int> range, IRuleset<TResult, TRule> ruleSet)
        {
            foreach (int i in range)
            {
                yield return ruleSet.Combine(i, ruleSet.Rules.Where(p => p.Item2(i)).Select(p => p.Item1(i)));
            }
        }
    }
}
