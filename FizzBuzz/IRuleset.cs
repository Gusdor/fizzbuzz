﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz
{
    public interface IRuleset<TResult, TRule>
    {
        /// <summary>
        /// Gets the pairs of rule matchers and result generators that define the value of each number.
        /// </summary>
        (Func<int, TRule>, Predicate<int>)[] Rules { get; }

        /// <summary>
        /// Combines the results of multiple matches rules into a single result.
        /// </summary>
        /// <param name="value">The value that was matched.</param>
        /// <param name="ruleOutput">The results of the matched rules.</param>
        /// <returns></returns>
        TResult Combine(int value, IEnumerable<TRule> ruleOutput);
    }
}
