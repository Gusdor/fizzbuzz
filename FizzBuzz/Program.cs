﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (var item in Game.Play(100))
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
